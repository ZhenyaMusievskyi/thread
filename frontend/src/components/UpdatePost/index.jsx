import { Button, Form, Icon, Image } from 'semantic-ui-react';
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

const UpdatePost = ({
  post,
  updatePost,
  uploadImage,
  close
}) => {
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState(post.image);
  const [isUploading, setIsUploading] = useState(false);

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    await updatePost({ ...post, body, image });
    close();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id, link } = await uploadImage(target.files[0]);
      setImage({ id, link });
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <Form onSubmit={handleUpdatePost}>
      <div className={styles.formContent}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.link && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.link} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit">Post</Button>
      </div>
    </Form>
  );
};

UpdatePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdatePost;
