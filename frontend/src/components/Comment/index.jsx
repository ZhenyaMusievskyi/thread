import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Dropdown, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import store from '../../store';

const Comment = ({ comment, likeComment, dislikeComment, deleteComment, setUpdatingCommentId }) => {
  const { id, body, createdAt, user, likeCount, dislikeCount } = comment;

  return (
    <CommentUI>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt)
            .fromNow()}
        </CommentUI.Metadata>
        {store.getState().profile.user?.id !== user.id ? '' : (
          <Dropdown>
            <Dropdown.Menu size="sm" title="">
              <Dropdown.Item onClick={() => setUpdatingCommentId(id)}>Edit</Dropdown.Item>
              <Dropdown.Item onClick={() => deleteComment(id)}>Delete</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        )}
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <Label basic size="small" as="a" onClick={() => likeComment(id)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" onClick={() => dislikeComment(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  setUpdatingCommentId: PropTypes.func.isRequired
};

export default Comment;
