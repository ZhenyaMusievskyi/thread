import { Button, Form } from 'semantic-ui-react';
import React, { useState } from 'react';
import PropTypes from 'prop-types';

const UpdateComment = ({ comment, updateComment, close }) => {
  const [body, setBody] = useState(comment.body);

  const handleUpdateComment = () => {
    if (!body) {
      return;
    }

    updateComment({ ...comment, body });
    close();
  };

  return (
    <Form reply onSubmit={handleUpdateComment}>
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content="Update comment" labelPosition="left" icon="edit" primary />
    </Form>
  );
};

UpdateComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdateComment;
