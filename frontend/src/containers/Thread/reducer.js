import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  SET_EXPANDED_POST,
  DELETE_POST, UPDATE_POST
} from './actionTypes';
import { SET_EXPANDED_POST_COMMENTS } from '../ExpandedPost/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case DELETE_POST:
      return {
        posts: state.posts.filter(post => post.id !== action.postId)
      };
    case UPDATE_POST:
      return {
        ...state
      };
    case SET_EXPANDED_POST_COMMENTS:
      return {
        posts: state.posts.map(post => (post.id !== action.post.id ? post : action.post)),
        expandedPost: action.post
      };
    default:
      return state;
  }
};
