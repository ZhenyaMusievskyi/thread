import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import { likePost, dislikePost, toggleExpandedPost,
  addComment, updatePost, deletePost } from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';
import * as imageService from 'src/services/imageService';
import { likeComment, dislikeComment, deleteComment, updateComment } from './action';
import UpdateComment from '../../components/UpdateComment';
import UpdatePost from '../../components/UpdatePost';

const ExpandedPost = ({
  post,
  sharePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add,
  updatePost: update,
  deletePost: deletion,
  likeComment: setLikeToComment,
  dislikeComment: setDislikeToComment,
  deleteComment: commentDeletion,
  updateComment: commentUpdating
}) => {
  const [updatingCommentId, setUpdatingCommentId] = useState('');
  const [updatingPost, setUpdatingPost] = useState(undefined);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            { updatingPost !== post ? (
              <Post
                post={post}
                likePost={like}
                dislikePost={dislike}
                toggleExpandedPost={toggle}
                sharePost={sharePost}
                updatePost={setUpdatingPost}
                deletePost={deletion}
              />
            ) : (
              <UpdatePost
                post={post}
                updatePost={update}
                uploadImage={uploadImage}
                close={() => setUpdatingPost(undefined)}
              />
            )}
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments.sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (updatingCommentId !== comment.id ? (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    likeComment={setLikeToComment}
                    dislikeComment={setDislikeToComment}
                    deleteComment={commentDeletion}
                    setUpdatingCommentId={setUpdatingCommentId}
                  />
                ) : (
                  <UpdateComment
                    comment={comment}
                    updateComment={commentUpdating}
                    close={() => setUpdatingCommentId(undefined)}
                  />
                )
                ))}
              <AddComment postId={post.id} addComment={add} />
            </CommentUI.Group>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updatePost,
  deletePost,
  likeComment,
  dislikeComment,
  deleteComment,
  updateComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
