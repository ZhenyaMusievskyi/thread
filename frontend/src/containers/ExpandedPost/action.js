import * as commentService from 'src/services/commentService';
import { SET_EXPANDED_POST_COMMENTS } from './actionTypes';

const setCommentsAction = post => ({
  type: SET_EXPANDED_POST_COMMENTS,
  post
});

const refreshComments = async (updatedComment, dispatch, getRootState) => {
  const { posts: { expandedPost: { comments } } } = getRootState();
  const { posts: { expandedPost } } = getRootState();
  expandedPost.comments = comments.map(comment => (comment.id !== updatedComment.id ? comment : updatedComment));
  dispatch(setCommentsAction(expandedPost));
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  await commentService.likeComment(commentId);
  const updatedComment = await commentService.getComment(commentId);
  refreshComments(updatedComment, dispatch, getRootState);
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  await commentService.dislikeComment(commentId);
  const updatedComment = await commentService.getComment(commentId);
  refreshComments(updatedComment, dispatch, getRootState);
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  commentService.deleteComment(commentId);
  const { posts: { expandedPost: { comments } } } = getRootState();
  const { posts: { expandedPost } } = getRootState();
  expandedPost.comments = comments.filter(comment => comment.id !== commentId);
  dispatch(setCommentsAction(expandedPost));
};

export const updateComment = comment => async (dispatch, getRootState) => {
  commentService.updateComment(comment);
  refreshComments(comment, dispatch, getRootState);
};
