package com.threadjava.comment.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CommentDeletionDto {
    UUID id;
}
