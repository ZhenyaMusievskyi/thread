package com.threadjava.commentReactions.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ReceivedCommentReactionDto {
    UUID commentId;
    UUID userId;
    Boolean isLike;
}
