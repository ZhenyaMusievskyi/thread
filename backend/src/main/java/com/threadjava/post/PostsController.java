package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue="0") Integer from,
                                 @RequestParam(defaultValue="10") Integer count,
                                 @RequestParam(required = false) UUID userId) {
        return postsService.getAllPosts(from, count, userId);
    }

    @GetMapping("/userfilter")
    public List<PostListDto> getFilteredPosts(@RequestParam(defaultValue="0") Integer from,
                                              @RequestParam(defaultValue="10") Integer count,
                                              @RequestParam(required = false) UUID userId) {
        return postsService.getAnotherUsersPost(from, count, userId);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @GetMapping("/liked")
    public List<PostListDto> getLikedPosts(@RequestParam(defaultValue="0") Integer from,
                                           @RequestParam(defaultValue="10") Integer count,
                                           @RequestParam(required = false) UUID userId) {
        var posts = postsService.getLikedPosts(from, count, userId);
        return posts;
    }

    @PostMapping("/postupdating")
    public void updatePost(@RequestBody PostUpdatingDto postDto) {
        postsService.updatePost(postDto);
    }

    @PostMapping("/postdeletion")
    public void deletePost(@RequestBody PostDeletionDto postDto) {
        postsService.softPostDeletion(postDto);
    }
}
